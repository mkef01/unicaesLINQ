﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LINQ_Ejemplo.Models
{
    public class VistaAutorLibro
    {
        public string autor { get; set; }
        public string titulo { get; set; }
    }
}