﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LINQ_Ejemplo.Models
{
    public class LibrosAlumno
    {
        public string Nombre { get; set; }
        public string Fecha { get; set; }
        public string Titulo { get; set; }
        public string Codigo { get; set; }
    }
}